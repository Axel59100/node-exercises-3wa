import express from "express";
import dotenv from 'dotenv';

dotenv.config();
const { APP_LOCALHOST : hostname, APP_PORT: port } = process.env;
const app = express();

app.use(express.static(__dirname + "/public"));

app.get('/', (req, res)=> {
  res.json({ message : "Hello World" });
})

app.listen(port, () => {
  console.log(`Example app listening at http://${hostname}:${port}`);
});
